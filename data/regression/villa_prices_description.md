# Villa prices in Saudi Arabia DataSet

## Источник:
> https://www.kaggle.com/maha48/villas-price-dataset

## Описание:
> В последнее время цены на недвижимость в Королевстве Саудовская Аравия выросли, особенно на виллы, и, поскольку жилье является одним из наиболее важных факторов жизни, этот файл помогает прогнозировать цены по каждому региону Саудовской Аравии и требует учитывать факторы, влияющие на рост цены виллы, такие как город, район и другие особенности каждой виллы.

> Эти данные были собраны с веб-сайта поиска недвижимости .
Эти данные были собраны в образовательных целях, а не для любого другого использования.

## Целевой признак:
> Прогнозирование стоимость вилл на основе их характеристик.

## Признаки:
> Feature | Type | Description | 
> --- | --- | --- |
> price | float | Villa price 
> neighborhood_name | string | neighborhood name 
> administrative_area | string | Administrative area name
> city | string | city name where villa
> rooms | int | number of bedrooms in villa
> bathrooms | int | number of bathrooms in villa
> sqm | int | sqm
> elevator | int | check if there is elevator or no. 1: there is elevator. 0: no elevator
> pool | int | check if there is pool or no. 1: there is pool. 0: no pool
> driver | int | check if there is driver room or no. 1: there is driver room. 0: no driver room
> garden | int | check if there is garden or no. 1: there is garden. 0: no garden
